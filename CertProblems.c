// 1.
#include <stdio.h>
const char *p;

void dont_do_this(void) {
    const char c_str[] = "This will change";
    p = c_str;
}

void innocuous(void) {
    printf("%s\n", p);
}

int main(void) {
    dont_do_this();
    innocuous();
    return 0;
}


//2.

char *init_array(void) {
    char array[10];
    /* Initialize array */
    return array;
}


//3.

void squirrel_away(char **ptr_param) {
    char local[10];
    /* Initialize array */
    *ptr_param = local;
}

void rodent(void) {
    char *ptr;
    squirrel_away(&ptr);
    /* ptr is live but invalid here */
}

//4.
#include <stddef.h>

int main(void) {
    for (size_t i = 0; i < 100; ++i) {
        /* int malloc() assumed */
        char *ptr = (char *) malloc(0x10000000);
        *ptr = 'a';
    }
    return 0;
}


//5.
#include <limits.h>
#include <stdio.h>

foo(void) {
    return UINT_MAX;
}

int main(void) {
    long long int c = foo();
    printf("%lld\n", c);
    return 0;
}


//6.
#include <stdlib.h>

struct flexArrayStruct {
    int num;
    int data[1];
};

void func(size_t array_size) {
    /* Space is allocated for the struct */
    struct flexArrayStruct *structP
            = (struct flexArrayStruct *)
            malloc(sizeof (struct flexArrayStruct)
            + sizeof (int) * (array_size - 1));
    if (structP == NULL) {
        /* Handle malloc failure */
    }
    structP->num = array_size;
    /*
     * Access data[] as if it had been allocated
     * as data[array_size].
     */
    for (size_t i = 0; i < array_size; ++i) {
        structP->data[i] = 1;
    }
}

//7. 
/* In a.c */
extern int i;

int f(void) {
    return ++i;
}
/* In b.c */
short i;

//8. 
/* In a.c */
extern int *a;

int f(unsigned int i, int x) {
    int tmp = a[i];
    a[i] = x;
    return tmp;
}
/* In b.c */
int a[] = {1, 2, 3, 4};


//9.
/* In a.c */
extern int f(int a); /* UB 15 */

int g(int a) {
    return f(a); /* UB 41 */
}

/* In b.c */
long f(long a) { /* UB 15 */
    return a * 2;
}



//10.
#include <stdio.h>
extern void f(int i);

void func(int expr) {
    switch (expr) {
            int i = 4;
            f(i);
        case 0:
            i = 17;
            /* Falls through into default code */
        default:
            printf("%d\n", i);
    }
}



//11.
#include <stdio.h>

void func(int i, int *b) {
    int a = i + b[++i];
    printf("%d, %d", a, i);
}


//12.
extern void func(int i, int j);

void f(int i) {
    func(i++, i);
}

//13.
extern void c(int i, int j);
int glob;

int a(void) {
    return glob + 10;
}

int b(void) {
    glob = 42;
    return glob;
}

void func(void) {
    c(a(), b());
}

//14.

void set_flag(int number, int *sign_flag) {
    if (NULL == sign_flag) {
        return;
    }

    if (number > 0) {
        *sign_flag = 1;
    } else if (number < 0) {
        *sign_flag = -1;
    }
}

int is_negative(int number) {
    int sign;
    set_flag(number, &sign);
    return sign < 0;
}


//15.
#include <png.h> /* From libpng */
#include <string.h>

void func(png_structp png_ptr, int length, const void *user_data) {
    png_charp chunkdata;
    chunkdata = (png_charp) png_malloc(png_ptr, length + 1);
    /* ... */
    memcpy(chunkdata, user_data, length);
    /* ... */
}

//16.
#include <string.h>
#include <stdlib.h>

void f(const char *input_str) {
    size_t size = strlen(input_str) + 1;
    char *c_str = (char *) malloc(size);
    memcpy(c_str, input_str, size);
    /* ... */
    free(c_str);
    c_str = NULL;
    /* ... */
}

//17.
#include <stdio.h>

struct X {
    char a[8];
};

struct X salutation(void) {
    struct X result = {"Hello"};
    return result;
}

struct X addressee(void) {
    struct X result = {"world"};
    return result;
}

int main(void) {
    printf("%s, %s!\n", salutation().a, addressee().a);
    return 0;
}



//18.
#include <stdio.h>

struct X {
    int a[6];
};

struct X addressee(void) {
    struct X result = {
        { 1, 2, 3, 4, 5, 6}
    };
    return result;
}

int main(void) {
    printf("%x", ++(addressee().a[0]));
    return 0;
}

//19.
#include <stdio.h>
#include <string.h>

char *(*fp)();

int main(void) {
    const char *c;
    fp = strchr;
    c = fp('e', "Hello");
    printf("%s\n", c);
    return 0;
}


//20.
#include <stdio.h>

void f(void) {
    if (sizeof (int) == sizeof (float)) {
        float f = 0.0f;
        int *ip = (int *) &f;
        (*ip)++;
        printf("float is %f\n", f);
    }
}


//21.
#include <string.h>

struct s {
    char c;
    int i;
    char buffer[13];
};

void compare(const struct s *left, const struct s *right) {
    if (0 == memcmp(left, right, sizeof (struct s))) {
        /* ... */
    }
}

//22.
int *restrict a;
int *restrict b;

extern int c[];

int main(void) {
    c[0] = 17;
    c[1] = 18;
    a = &c[0];
    b = &c[1];
    a = b;
    /* ... */
}

//23.
#include <stddef.h>

void f(size_t n, int *restrict p, const int *restrict q) {
    while (n-- > 0) {
        *p++ = *q++;
    }
}

void g(void) {
    extern int d[100];
    /* ... */
    f(50, d + 1, d);
}

//24. 
#include <stdio.h>

void func(void) {
    int a = 14;
    int b = sizeof (a++);
    printf("%d, %d\n", a, b);
}

//25.
#include <stddef.h>
#include <stdio.h>

void f(size_t n) {
    size_t a = sizeof (int[++n]);

    size_t b = sizeof (int[++n % 1 + 1]);

    printf("%zu, %zu, %zu\n", a, b, n);
    /* ... */
}

//26.
#include <stdio.h>

#define S(val) _Generic(val, int : 2, \
                             short : 3, \
                             default : 1)

void func(void) {
    int a = 0;
    int b = S(a++);
    printf("%d, %d\n", a, b);
}

//27.
#include <stdio.h>

void func(void) {
    int val = 0;
    /* ... */
    size_t align = _Alignof(int[++val]);
    printf("%zu, %d\n", align, val);
    /* ... */
}


//28.
if (a = b) {
    /* ... */
}

//29.
do {
    /* ... */
} while (foo(), x = y);

for (; x; foo(), x = y) {
    /* ... */
}

do {
    /* ... */
} while (x = y, p = q);

while (ch = '\t' || ch == ' ' || ch == '\n') {
    /* ... */
}

//30.
if (!(getuid() & geteuid() == 0)) {
    /* ... */
}


//31.

void func(unsigned int ui_a, unsigned int ui_b) {
    unsigned int usum = ui_a + ui_b;
    /* ... */
}

//32.
#include <limits.h>

void func(void) {
    unsigned long int u_a = ULONG_MAX;
    signed char sc;
    sc = (signed char) u_a;
    /* ... */
}


//33.
#include <limits.h>

void func(void) {
    signed int si = INT_MIN;
    /* Cast eliminates warning */
    unsigned int ui = (unsigned int) si;

    /* ... */
}


//34. 

void func(signed int si_a, signed int si_b) {
    signed int sum = si_a + si_b;
    /* ... */
}


//35.
#include <limits.h>

void func(signed long s_a, signed long s_b) {
    signed long result;
    if ((s_a == LONG_MIN) && (s_b == -1)) {
        /* Handle error */
    } else {
        result = s_a / s_b;
    }
    /* ... */
}


//36.

enum {
    TABLESIZE = 100
};

static int table[TABLESIZE];

int *f(int index) {
    if (index < TABLESIZE) {
        return table + index;
    }
    return NULL;
}


//37. 
#include <stddef.h>
#define COLS 5
#define ROWS 7
static int matrix[ROWS][COLS];

void init_matrix(int x) {
    for (size_t i = 0; i < COLS; i++) {
        for (size_t j = 0; j < ROWS; j++) {
            matrix[i][j] = x;
        }
    }
}


//38.
#include <stddef.h>

enum {
    SIZE = 32
};

void func(void) {
    int nums[SIZE];
    int end;
    int *next_num_ptr = nums;
    size_t free_elements;

    /* Increment next_num_ptr as array fills */

    free_elements = &end - next_num_ptr;
}


//39.

struct numbers {
    short num_a, num_b, num_c;
};

int sum_numbers(const struct numbers *numb) {
    int total = 0;
    const short *numb_ptr;

    for (numb_ptr = &numb->num_a;
            numb_ptr <= &numb->num_c;
            numb_ptr++) {
        total += *(numb_ptr);
    }

    return total;
}

int main(void) {
    struct numbers my_numbers = {1, 2, 3};
    sum_numbers(&my_numbers);
    return 0;
}




//40.

enum {
    INTBUFSIZE = 80
};

extern int getdata(void);
int buf[INTBUFSIZE];

void func(void) {
    int *buf_ptr = buf;

    while (buf_ptr < (buf + sizeof (buf))) {
        *buf_ptr++ = getdata();
    }
}


//41.
char *str = "string literal";
str[0] = 'S';


//42.
#include <stddef.h>

void copy(size_t n, char src[n], char dest[n]) {
    size_t i;

    for (i = 0; src[i] && (i < n); ++i) {
        dest[i] = src[i];
    }
    dest[i] = '\0';
}

//43.

#include <stdio.h>

void func(void) {
    char c_str[3] = "abc";
    printf("%s\n", c_str);
}



//44.
#include <stdlib.h>

struct node {
    int value;
    struct node *next;
};

void free_list(struct node *head) {
    for (struct node *p = head; p != NULL; p = p->next) {
        free(p);
    }
}

//45.
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    char *return_val = 0;
    const size_t bufsize = strlen(argv[0]) + 1;
    char *buf = (char *) malloc(bufsize);
    if (!buf) {
        return EXIT_FAILURE;
    }
    /* ... */
    free(buf);
    /* ... */
    strcpy(buf, argv[0]);
    /* ... */
    return EXIT_SUCCESS;
}



//46.
#include <stdlib.h>

enum {
    BUFFER_SIZE = 32
};

int f(void) {
    char *text_buffer = (char *) malloc(BUFFER_SIZE);
    if (text_buffer == NULL) {
        return -1;
    }
    return 0;
}


//47.
#include <stddef.h>

struct flex_array_struct {
    size_t num;
    int data[];
};

void func(void) {
    struct flex_array_struct flex_struct;
    size_t array_size = 4;

    /* Initialize structure */
    flex_struct.num = array_size;

    for (size_t i = 0; i < array_size; ++i) {
        flex_struct.data[i] = 0;
    }
}



//48.
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

enum {
    MAX_ALLOCATION = 1000
};

int main(int argc, const char *argv[]) {
    char *c_str = NULL;
    size_t len;

    if (argc == 2) {
        len = strlen(argv[1]) + 1;
        if (len > MAX_ALLOCATION) {
            /* Handle error */
        }
        c_str = (char *) malloc(len);
        if (c_str == NULL) {
            /* Handle error */
        }
        strcpy(c_str, argv[1]);
    } else {
        c_str = "usage: $>a.exe [string]";
        printf("%s\n", c_str);
    }
    free(c_str);
    return 0;
}















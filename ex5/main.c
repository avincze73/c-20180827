/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on August 28, 2018, 1:24 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int accum2(int* begin, int* end, int init,
        int (*operation)(int, int),
        bool(*condition)(int)){
    for(;begin!=end;++begin){
        if(condition(*begin)){
            init = operation(init, *begin);
        }
    }
    return init;
}

int accum1(int* array, int length, int init, 
        int (*operation)(int, int)){
    for(int i=0; i != length;++i){
        init = operation(init, i[array]);
    }
    return init;
}

int accum(int* array, int length, int init){
    for(int i=0; i != length;++i){
        init += i[array];
    }
    return init;
}
/*
 * 
 */

int add(int a, int b){
    return a + b;
}
int multiply(int a, int b){
    return a * b;
}

bool even(int i){
    return i % 2 == 0;
}

bool positive (int i){
    return i > 0;
}
int main(int argc, char** argv) {

    int a[] = {1,2,3,4,5,6,7,8,9,10};
    printf("%d\n", accum(a, 10, 0));
    
    printf("%d\n", accum1(a, 10, 0, add));
    printf("%d\n", accum1(a, 10, 1,  multiply));
    
    
    printf("%d\n", accum2(a, a+10, 0, add, positive));
    printf("%d\n", accum2(a, a+10, 1,  multiply, even));
    return (EXIT_SUCCESS);
}


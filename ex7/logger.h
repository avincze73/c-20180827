/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   logger.h
 * Author: avincze
 *
 * Created on August 30, 2018, 10:05 AM
 */

#ifndef LOGGER_H
#define LOGGER_H
#include "types.h"

void attach(mylogger);
void detach(mylogger);
void LOG(const char *, SEVERITY);

#endif /* LOGGER_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on August 30, 2018, 10:04 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "logger.h"

/*
 * 
 */

void consoleLogger(const char* message, SEVERITY severity){
    printf("CONSOLE-LOGGER\t%s\t%s\n", severity == INFO ? "INFO" : "ERROR", message);
}

void fileLogger(const char* message, SEVERITY severity){
    printf("FILE-LOGGER\t%s\t%s\n", severity == INFO ? "INFO" : "ERROR", message);    
}

void emailLogger(const char* message, SEVERITY severity){
    printf("EMAIL-LOGGER\t%s\t%s\n", severity == INFO ? "INFO" : "ERROR", message);
}

int main(int argc, char** argv) {

    attach(consoleLogger);
    attach(fileLogger);
    LOG("First info", INFO);
    LOG("First error", ERROR);
    detach(fileLogger);
    LOG("Second info", INFO);
    LOG("Second error", ERROR);

    return (EXIT_SUCCESS);
}


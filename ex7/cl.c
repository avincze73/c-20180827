/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cl.h"
#include <stdlib.h>

static struct chain * first = NULL;

static struct chain * getLast(){
    struct chain * top = getFirst();
    
    while(top->next != NULL){
        top = top->next;
    }
    return top;
}

void insert(mylogger logger){
    if (first == NULL) {
        struct chain* c = (struct chain *)malloc(sizeof(struct chain));
        first = c;
        first->next = NULL;
        first->l = logger;
    } else {
        struct chain* last = getLast();
        struct chain* c = (struct chain *)malloc(sizeof(struct chain));
        last->next = c;
        c->next = NULL;
        c->l = logger;
    }
}

void remove(mylogger logger){
    if (first == NULL) {
        return;
    }
    
    struct chain * top = getFirst();
    
    while(top->next != NULL && top->l != logger){
        top = top->next;
    }
    if (top->l == logger) {
        top->l = NULL;
    }
}

struct chain* getFirst(){
    return first;
}

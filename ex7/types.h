/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   types.h
 * Author: avincze
 *
 * Created on August 30, 2018, 10:16 AM
 */

#ifndef TYPES_H
#define TYPES_H


typedef enum {INFO, ERROR} SEVERITY;

typedef void (*mylogger)(const char*, SEVERITY);

#endif /* TYPES_H */


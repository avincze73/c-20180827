/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "logger.h"
#include "cl.h"
#include <stdlib.h>

void attach(mylogger logger) {
    insert(logger);
}

void detach(mylogger logger) {
    remove(logger);
}

void LOG(const char * message, SEVERITY severity) {
    struct chain * first = getFirst();
    if (first == NULL) {
        return;
    }
    do {
        if (first->l != NULL) {
            first->l(message, severity);
        }
        first = first->next;
    } while (first != NULL);
}



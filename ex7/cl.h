/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cl.h
 * Author: avincze
 *
 * Created on August 30, 2018, 10:05 AM
 */

#ifndef CL_H
#define CL_H

#include "types.h"
struct chain;

struct chain {
    mylogger l;
    struct chain * next;
};

void insert(mylogger);
void remove(mylogger);
struct chain* getFirst();

#endif /* CL_H */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "StringUtils.h"
#include <string.h>

bool isMirror(const char* word){
    for(int i = 0, j = strlen(word) -1; i < j; i++, j--)
    {
        if (word[i] != j[word]) {
            return false;
        }
    }
    return true;
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on August 30, 2018, 1:12 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "PrintUtils.h"
#include "StringUtils.h"


/*
 * 
 */
int main(int argc, char** argv) {

    printN("started");
    printN(isMirror("abcba") == true ?"yes":"no");
    printN("finished");
    return (EXIT_SUCCESS);
}


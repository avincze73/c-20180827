/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on August 28, 2018, 10:55 AM
 */

#include <stdio.h>
#include <stdlib.h>

void freeNumbers(int** p){
    for(int i = 0; i < 4; i++){
        free(*(p+i));
    }
    free(p);
}

int** getNumbers(){
    int** result = (int**)malloc(4 * sizeof(int*));
    
    *result = (int*)malloc(1*sizeof(int));
    **result = 1;
    
    *(result+1) = (int*)malloc(2*sizeof(int));
    **(result+1) = 2;
    *(*(result+1)+1) = 3;
    
    *(result+2) = (int*)malloc(3*sizeof(int));
    **(result+2) = 4;
    *(*(result+2)+1) = 5;
    *(*(result+2)+2) = 6;
    
    
    *(result+3) = (int*)malloc(4*sizeof(int));
    **(result+3) = 7;
    *(*(result+3)+1) = 8;
    *(*(result+3)+2) = 9;
    *(*(result+3)+3) = 10;
    
    return result;
}

void getNumbers2(int*** numbers){
    *numbers = (int**)malloc(4 * sizeof(int*));
    
    
    
    (*numbers)[0] = (int*)malloc(1*sizeof(int));
    *(*numbers)[0] = 1;
    
    
    
    
    
    *((*numbers)+1) = (int*)malloc(2*sizeof(int));
    *(*numbers)[1] = 2;
    *((*numbers)[1]+1) = 3;
    
    (*numbers)[2] = (int*)malloc(3*sizeof(int));
    *(*numbers)[2] = 4;
    *((*numbers)[2]+1) = 5;
    *((*numbers)[2]+2) = 6;
    
    
    (*numbers)[3] = (int*)malloc(4*sizeof(int));
    *(*numbers)[3] = 7;
    *((*numbers)[3]+1) = 8;
    *((*numbers)[3]+2) = 9;
    *((*numbers)[3]+3) = 10;
}

/*
 * 
 */
int main(int argc, char** argv) {
    //int** numbers = getNumbers();
    int** numbers;
    getNumbers2(&numbers);
    printf("%d", *(*(numbers+0)+0));
    freeNumbers(numbers);
    return (EXIT_SUCCESS);
}

